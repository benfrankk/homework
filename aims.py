import math

def mean(numbers):
    try:
        return sum(numbers) / float(len(numbers))
    except ZeroDivisionError:
        return 0
    except TypeError:
        print "THE ARGUMENT MUST BE A LIST"

def std(numbers):
    sums = 0
    means = mean(numbers)
    for i in range(len(numbers)) :
        sums = sums + (numbers[i] - means) ** 2
    sum_mean = sums / len(numbers)
    stds = math.sqrt(sum_mean)
    return stds

def avg_range(filenames):
    list_range = []
    for fil in filenames :
        f = open(fil)
        line_range = f.readlines()[7]
        line_range = line_range.strip()
        list_range.append(float(line_range[7:]))
    return sum(list_range) / float(len(list_range))

def main():
    "Welcome to the AIMS module"

if __name__ == "__main__":
    main()
